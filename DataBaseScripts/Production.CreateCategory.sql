USE AdventureWorks2019
GO
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'Production'
     AND SPECIFIC_NAME = N'CreateCategory' 
)
   DROP PROCEDURE Production.CreateCategory
GO
CREATE PROCEDURE Production.CreateCategory
@Name NVARCHAR(50)
AS
INSERT INTO Production.ProductCategory([Name])VALUES(@Name)
GO