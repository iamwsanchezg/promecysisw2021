USE AdventureWorks2019
GO
/**
* El presente diseño es presentado con fines ilustrativos exclusivamente.
* No debe considerarse completamente seguro para un ambiente de producción.
* Para un ambiente de producción debe considerarse el uso de mecanismos de recuperación de contraseñas,
* reinicio de contraseña, protección de contraseñas con sal, autenticación con verificación en dos pasos, etc.
**/
CREATE SCHEMA [Security]
GO

CREATE TABLE [Security].[Users]
(
	Id INT IDENTITY(1,1) NOT NULL,
	UserName VARCHAR(256) NOT NULL,
	[Password] VARBINARY(130) NOT NULL,
	CONSTRAINT PK_Security_Users PRIMARY KEY CLUSTERED (Id ASC),
	CONSTRAINT UK_Security_Users_UserName UNIQUE(UserName)
)
GO

IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'Security'
     AND SPECIFIC_NAME = N'CreateUser' 
)
   DROP PROCEDURE [Security].CreateUser
GO
/**
** Descripcion: Permite registrar un nuevo usuario
**/
CREATE PROCEDURE [Security].CreateUser
@UserName VARCHAR(256),
@Password VARCHAR(50),
@RETURN TINYINT OUTPUT,
@RETURN_MESSAGE NVARCHAR(500) OUTPUT	
AS
IF NOT EXISTS(SELECT 1 FROM [Security].Users WHERE UserName = @UserName)
BEGIN
	INSERT INTO [Security].Users(UserName,[Password]) 
	VALUES(@UserName,HASHBYTES('SHA2_512',@Password))
	--
	SET @RETURN = 0;
	SET @RETURN_MESSAGE = 'Usuario agregado exitosamente.';
END
ELSE
BEGIN
	SET @RETURN = 1;
	SET @RETURN_MESSAGE = 'El usuario ' + @UserName + ' ya existe.';
END
GO

IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'Security'
     AND SPECIFIC_NAME = N'VerifyUser' 
)
   DROP PROCEDURE [Security].VerifyUser
GO
CREATE PROCEDURE [Security].VerifyUser
@UserName VARCHAR(256),
@Password VARCHAR(50),
@RETURN TINYINT OUTPUT,
@RETURN_MESSAGE NVARCHAR(500) OUTPUT	
AS
IF EXISTS(SELECT 1 FROM [Security].Users WHERE UserName = @UserName AND [Password] = HASHBYTES('SHA2_512',@Password))
BEGIN
	SET @RETURN = 0;
	SET @RETURN_MESSAGE = 'El usuario y contraseñas son correctos';
END
ELSE
BEGIN
	SET @RETURN = 1;
	SET @RETURN_MESSAGE = 'El usuario o constraseña es incorrecto.';
END
GO

--DECLARE @r TINYINT,
--		@m NVARCHAR(500);
--EXEC [Security].CreateUser 'wsanchez', 'Abc123.', @r output, @m output;
--SELECT @m;
--GO



--DECLARE @r TINYINT,
--		@m NVARCHAR(500);
--EXEC [Security].VerifyUser 'wsanchez', 'Abc123.', @r output, @m output;
--SELECT @m;
--GO