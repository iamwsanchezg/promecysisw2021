﻿using AwApp.Controllers;
using Entities.Security;
using System.Windows;
using System.Windows.Input;

namespace AwApp.Views.Windows
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private LoginController lc;

        public LoginWindow()
        {
            InitializeComponent();
            SetupController();
        }

        public void SetupController()
        {
            lc = new LoginController(this);
            RoutedEventHandler routed = new RoutedEventHandler(lc.ButtonEventHandler);
            this.LoginButton.Click += routed;
            this.CloseButton.Click += routed;
            KeyEventHandler keyEvent = new KeyEventHandler(lc.OnKeyUpHandler);
            this.PasswordTextBox.KeyUp += keyEvent;
        }

        public User GetData()
        {
            User u = new User
            {
                UserName = UserTextBox.Text.Trim(),
                Password = PasswordTextBox.Password.Trim()
            };
            return u;
        }
    }
}