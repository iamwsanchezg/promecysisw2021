﻿using AwApp.Controllers;
using AwApp.Properties;
using Entities.General;
using Lib.Security;
using System;
using System.Windows;

namespace AwApp.Views.Windows
{
    /// <summary>
    /// Interaction logic for ConfigurationWindow.xaml
    /// </summary>
    public partial class ConfigurationWindow : Window
    {
        private ConfigurationController cc;

        public ConfigurationWindow()
        {
            InitializeComponent();
            SetupController();
            ShowSettings();
        }

        public void SetupController()
        {
            cc = new ConfigurationController(this);
            RoutedEventHandler routed = new RoutedEventHandler(cc.ButtonEventHandler);
            OkButton.Click += routed;
            CloseButton.Click += routed;
        }

        public void ShowSettings()
        {
            try
            {
                string phrase = Environment.MachineName + Environment.UserName;
                ServerTextBox.Text = Settings.Default.ServerName;
                DataBaseTextBox.Text = Settings.Default.DataBaseName;
                UserTextBox.Text = Settings.Default.UserName;
                PasswordTextBox.Password = StringCipher.Decrypt(Settings.Default.Password, phrase);
            }
            catch (Exception ex) { Console.WriteLine(ex.StackTrace); }
        }

        public ApplicationSettings GetData()
        {
            ApplicationSettings settings = new ApplicationSettings
            {
                ServerName = ServerTextBox.Text.Trim(),
                DataBaseName = DataBaseTextBox.Text.Trim(),
                UserName = UserTextBox.Text.Trim(),
                Password = PasswordTextBox.Password
            };
            return settings;
        }
    }
}