﻿using AwApp.Controllers;
using System.Windows;

namespace AwApp.Views.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowController mc;

        public MainWindow()
        {
            InitializeComponent();
            SetupController();
        }

        public void SetupController()
        {
            mc = new MainWindowController(this);
            Loaded += new RoutedEventHandler(mc.MainWindowEventHandler);
            RoutedEventHandler routed = new RoutedEventHandler(mc.MainMenuEventHandler);
            SettingsMenutItem.Click += routed;
            exitAplicationMenutItem.Click += routed;
            CategoryMenuItem.Click += routed;
            SubCategoryMenuItem.Click += routed;
        }
    }
}