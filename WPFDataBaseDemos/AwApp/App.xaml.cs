﻿using AwApp.Properties;
using System;
using System.Windows;

namespace AwApp
{
    /// <summary>
    /// Interação lógica para App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            if (ConfigurationExists())
            {
                this.StartupUri = new Uri("Views/Windows/LoginWindow.xaml", UriKind.Relative);
            }
            else
            {
                this.StartupUri = new Uri("Views/Windows/ConfigurationWindow.xaml", UriKind.Relative);
            }
        }

        protected bool ConfigurationExists()
        {
            bool exists = !string.IsNullOrEmpty(Settings.Default.ServerName)
                            && !string.IsNullOrEmpty(Settings.Default.DataBaseName)
                            && !string.IsNullOrEmpty(Settings.Default.UserName)
                            && !string.IsNullOrEmpty(Settings.Default.Password);
            return exists;
        }
    }
}