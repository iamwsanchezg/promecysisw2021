﻿using AwApp.Properties;
using AwApp.Views.Windows;
using DataAccess.Security;
using Entities.General;
using Entities.Security;
using Lib.Runtime;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AwApp.Controllers
{
    public class LoginController
    {
        private LoginWindow login;

        public LoginController(LoginWindow login)
        {
            this.login = login;
        }
        public void OnKeyUpHandler(object sender, KeyEventArgs e)
        {
            PasswordBox textBox = (PasswordBox)sender;
            if (textBox.Name.Equals("PasswordTextBox"))
            {
                if (e.Key == Key.Return)
                {
                    ExecuteLogin();
                }
            }
        }
        public void ButtonEventHandler(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            switch (button.Name)
            {
                case "CloseButton":
                    Application.Current.Shutdown();
                    break;

                case "LoginButton":
                    ExecuteLogin();
                    break;
            }
        }

        private void ExecuteLogin()
        {
            User u = login.GetData();
            Result r = Login(u);
            if (r.Outcome)
            {
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                login.Close();
            }
            else
            {
                MessageBox.Show(r.Message, "Login", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        public Result Login(User user)
        {
            ApplicationSettings settings = new ApplicationSettings(Settings.Default);
            DbUser db = new DbUser(settings.GetConnectionString());
            return db.VerifyUser(user);
        }
    }
}