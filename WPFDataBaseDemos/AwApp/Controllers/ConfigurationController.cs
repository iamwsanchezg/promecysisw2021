﻿using AwApp.Properties;
using AwApp.Views.Windows;
using DataAccess.General;
using Entities.General;
using Lib.Runtime;
using Lib.Security;
using System;
using System.Windows;
using System.Windows.Controls;

namespace AwApp.Controllers
{
    public class ConfigurationController
    {
        private ConfigurationWindow configuration;

        public ConfigurationController(ConfigurationWindow configuration)
        {
            this.configuration = configuration;
        }

        public void ButtonEventHandler(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            switch (button.Name)
            {
                case "CloseButton":
                    Application.Current.Shutdown();
                    break;

                case "OkButton":
                    Save();
                    break;
            }
        }

        public void Save()
        {
            ApplicationSettings settings = configuration.GetData();
            Result response;
            DbConnectivity db = new DbConnectivity(settings);
            response = db.CanIConnect();
            if (response.Outcome)
            {
                response = SaveSettings(settings);
                _ = MessageBox.Show(response.Message, "Configuración", MessageBoxButton.OK, response.Outcome ? MessageBoxImage.Information : MessageBoxImage.Exclamation);
                if (response.Outcome)
                {
                    LoginWindow login = new LoginWindow();
                    login.Show();
                    configuration.Close();
                }
            }
            else
            {
                _ = MessageBox.Show(response.Message, "Configuración", MessageBoxButton.OK, response.Outcome ? MessageBoxImage.Information : MessageBoxImage.Exclamation);
            }
        }

        public Result SaveSettings(ApplicationSettings settings)
        {
            Result response = new Result();
            try
            {
                string phrase = Environment.MachineName + Environment.UserName;
                Settings.Default.ServerName = settings.ServerName;
                Settings.Default.DataBaseName = settings.DataBaseName;
                Settings.Default.UserName = settings.UserName;
                Settings.Default.Password = StringCipher.Encrypt(settings.Password, phrase);
                Settings.Default.Save();
                response.Outcome = true;
                response.Message = "La configuración ha sido completada exitosamente.";
            }
            catch (Exception ex)
            {
                response.Outcome = false;
                response.Message = ex.Message;
                Console.WriteLine(ex.StackTrace);
            }
            return response;
        }
    }
}