﻿using AwApp.Views.Windows;
using System.Windows;
using System.Windows.Controls;

namespace AwApp.Controllers
{
    public class MainWindowController
    {
        private MainWindow window;

        public MainWindowController(MainWindow window)
        {
            this.window = window;
        }

        public void MainWindowEventHandler(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }

        public void MainMenuEventHandler(object sender, RoutedEventArgs e)
        {
            MenuItem Option = (MenuItem)sender;
            switch (Option.Name)
            {
                case "exitAplicationMenutItem":
                    Application.Current.Shutdown();
                    break;

                default:
                    break;
            }
        }
    }
}