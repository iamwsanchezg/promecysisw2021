﻿using DataAccess.General;
using Entities.Security;
using Lib.Runtime;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess.Security
{
    public class DbUser : DbConnectivity, ICrud<User>
    {
        public DbUser(string cx) : base(cx)
        {
            ConnectionString = cx;
        }

        public Result Create(User Input)
        {
            Result response = new Result();
            try
            {
                using (Connection = GetAppConnection())
                {
                    Connection.Open();
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = Connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Security.CreateUser";
                        command.Parameters.AddWithValue("UserName", Input.UserName);
                        command.Parameters.AddWithValue("Password", Input.Password);
                        SqlParameter r = command.Parameters.Add("RETURN", SqlDbType.TinyInt);
                        r.Direction = ParameterDirection.Output;
                        SqlParameter m = command.Parameters.Add("RETURN_MESSAGE", SqlDbType.NVarChar, 500);
                        m.Direction = ParameterDirection.Output;
                        response.Outcome = Convert.ToInt32(r.Value) == 0;
                        response.Message = Convert.ToString(m.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                response.Outcome = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public Result Delete(User Input)
        {
            throw new NotImplementedException();
        }

        public List<User> Get()
        {
            throw new NotImplementedException();
        }

        public User Get(int Id)
        {
            throw new NotImplementedException();
        }

        public Result Update(User Input)
        {
            throw new NotImplementedException();
        }

        public Result VerifyUser(User Input)
        {
            Result response = new Result();
            try
            {
                using (Connection = GetAppConnection())
                {
                    Connection.Open();
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = Connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Security.VerifyUser";
                        _ = command.Parameters.AddWithValue("UserName", Input.UserName);
                        _ = command.Parameters.AddWithValue("Password", Input.Password);
                        _ = command.Parameters.Add("RETURN", SqlDbType.TinyInt);
                        command.Parameters["RETURN"].Direction = ParameterDirection.Output;
                        _ = command.Parameters.Add("RETURN_MESSAGE", SqlDbType.NVarChar, 500);
                        command.Parameters["RETURN_MESSAGE"].Direction = ParameterDirection.Output;
                        _ = command.ExecuteNonQuery();
                        response.Outcome = (byte)command.Parameters["RETURN"].Value == 0;
                        response.Message = Convert.ToString(command.Parameters["RETURN_MESSAGE"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                response.Outcome = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}