﻿using DataAccess.General;
using Entities.Production;
using Lib.Runtime;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess.Production
{
    public class DbCategory : DbConnectivity, ICrud<Category>
    {
        public DbCategory(string cx) : base(cx)
        {
            ConnectionString = cx;
        }

        public Result Create(Category Input)
        {
            Result response = new Result();
            try
            {
                using (Connection = GetAppConnection())
                {
                    Connection.Open();
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = Connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Production.CreateCategory";
                        command.Parameters.AddWithValue("Name", Input.Name);
                        response.Outcome = command.ExecuteNonQuery() > 0;
                        response.Message = "Categoría registrada exitosamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Outcome = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public Result Delete(Category Input)
        {
            throw new System.NotImplementedException();
        }

        public List<Category> Get()
        {
            throw new System.NotImplementedException();
        }

        public Category Get(int Id)
        {
            throw new System.NotImplementedException();
        }

        public Result Update(Category Input)
        {
            Result response = new Result();
            try
            {
                using (Connection = GetAppConnection())
                {
                    Connection.Open();
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = Connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Production.UpdateCategory";
                        command.Parameters.AddWithValue("ProductCategoryID", Input.ProductCategoryID);
                        command.Parameters.AddWithValue("Name", Input.Name);
                        response.Outcome = command.ExecuteNonQuery() > 0;
                        response.Message = "Categoría registrada exitosamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Outcome = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}