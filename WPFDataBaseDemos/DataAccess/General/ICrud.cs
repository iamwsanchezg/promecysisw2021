﻿using Lib.Runtime;
using System.Collections.Generic;

namespace DataAccess.General
{
    public interface ICrud<T>
    {
        List<T> Get();

        T Get(int Id);

        Result Create(T Input);

        Result Update(T Input);

        Result Delete(T Input);
    }
}