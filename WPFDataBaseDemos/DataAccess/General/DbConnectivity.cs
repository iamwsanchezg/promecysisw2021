﻿using Entities.General;
using Lib.Runtime;
using System;
using System.Data.SqlClient;

namespace DataAccess.General
{
    public class DbConnectivity
    {
        public SqlConnection Connection { get; set; }
        public string ConnectionString { get; set; }

        public DbConnectivity(string cx)
        {
            ConnectionString = cx;
        }

        public DbConnectivity(ApplicationSettings settings)
        {
            ConnectionString = settings.GetConnectionString();
        }

        protected SqlConnection GetAppConnection()
        {
            return new SqlConnection(ConnectionString);
        }

        public Result CanIConnect()
        {
            Result result = new Result();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    result.Outcome = true;
                }
            }
            catch (Exception ex)
            {
                result.Outcome = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}