﻿namespace Lib.Runtime
{
    public class Result
    {
        public int Id { get; set; }
        public bool Outcome { get; set; }
        public string Message { get; set; }
    }
}