﻿using System;

namespace Entities.Production
{
    public class Category
    {
        public int ProductCategoryID { get; set; }
        public string Name { get; set; }
        public Guid RowGuid { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}