﻿using Lib.Security;
using System;
using System.Configuration;

namespace Entities.General
{
    public class ApplicationSettings
    {
        public ApplicationSettings()
        {
        }

        public ApplicationSettings(SettingsBase settings)
        {
            string phrase = Environment.MachineName + Environment.UserName;
            ServerName = (string)(settings["ServerName"]);
            DataBaseName = (string)(settings["DataBaseName"]);
            UserName = (string)(settings["UserName"]);
            Password = StringCipher.Decrypt((string)settings["Password"], phrase);
        }

        public string ServerName { get; set; }
        public string DataBaseName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string GetConnectionString()
        {
            return string.Format(@"Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3}", ServerName, DataBaseName, UserName, Password);
        }
    }
}